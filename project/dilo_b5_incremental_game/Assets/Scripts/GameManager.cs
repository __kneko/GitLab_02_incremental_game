﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour{
    private static GameManager _instance = null;

    // quit button handler
    public Button QuitButton;

    public static GameManager Instance{
        get{
            if (_instance == null)
                _instance = FindObjectOfType<GameManager>();
            return _instance;
        }
    }

    [Range(0f, 1f)]
    public float AutoCollectPercentage = 0.1f;
    public ResourceConfig[] ResourcesConfigs;
    public Sprite[] ResourcesSprites;

    public Transform ResourcesParent;
    public ResourceController ResourcePrefab;
    public TapText TapTextPrefab;

    public Transform CoinIcon;
    public Text GoldInfo;
    public Text AutoCollectInfo;

    private List<ResourceController> _activeResources = new List<ResourceController>();
    private List<TapText> _tapTextPool = new List<TapText>();
    private float _collectSecond;

    // must set to public (ResourceController need access)
    public double _totalGold { 
        get; private set;
    }

    private void Start(){
        AddAllResources();
    }

    private void Update(){
        // execute CollectPerSecond every second
        _collectSecond += Time.unscaledDeltaTime;
        if (_collectSecond >= 1f){
            CollectPerSecond();
            _collectSecond = 0f;
        }

        CheckResourceCost();

        CoinIcon.transform.localScale = Vector3.LerpUnclamped(CoinIcon.transform.localScale, Vector3.one * 2f, 0.15f);
        CoinIcon.transform.Rotate(0f, 0f, Time.deltaTime * -100f);
    }

    private void AddAllResources(){
        bool showResources = true;

        foreach (ResourceConfig config in ResourcesConfigs){
            GameObject obj = Instantiate(ResourcePrefab.gameObject, ResourcesParent, false);
            ResourceController resource = obj.GetComponent<ResourceController>();

            resource.SetConfig(config);
            obj.gameObject.SetActive(showResources);

            if (showResources && !resource.IsUnlocked)
                showResources = false;

            _activeResources.Add(resource);

            // button image handler (without this the button doesn't look/feel responsive)
            // enables the tint when the button is hovered or pressed
            resource.ResourceButton.image = resource.ResourceImage;
        }
    }

    public void ShowNextResource(){
        foreach (ResourceController resource in _activeResources){
            if (!resource.gameObject.activeSelf){
                resource.gameObject.SetActive(true);
                break;
            }
        }
    }

    private void CheckResourceCost(){
        foreach (ResourceController resource in _activeResources){
            // bool isBuyable = _totalGold >= resource.GetUpgradeCost();
            bool isBuyable = false;

            if (resource.IsUnlocked)
                isBuyable = _totalGold >= resource.GetUpgradeCost();
            else
                isBuyable = _totalGold >= resource.GetUnlockCost();

            resource.ResourceImage.sprite = ResourcesSprites[isBuyable ? 1 : 0];
        }
    }

    private void CollectPerSecond(){
        double output = 0;

        foreach (ResourceController resource in _activeResources){
            if (resource.IsUnlocked)
                output += resource.GetOutput();
        }

        output *= AutoCollectPercentage;
        // ToString("F1") rounding decimal number to 1 digit after comma
        // what's the $ for?
        AutoCollectInfo.text = $"Auto Collect: { output.ToString("F1") } / second";

        AddGold(output);
    }

    // must set to public (ResourceController need access)
    public void AddGold(double value){
        _totalGold += value;
        GoldInfo.text = $"Gold: { _totalGold.ToString("0") }";
    }

    public void CollectByTap(Vector3 tapPosition, Transform parent){
        double output = 0;

        foreach (ResourceController resource in _activeResources){
            if (resource.IsUnlocked)
                output += resource.GetOutput();
        }

        TapText tapText = GetOrCreateTapText();
        tapText.transform.SetParent(parent, false);
        tapText.transform.position = tapPosition;

        tapText.Text.text = $"+{ output.ToString("0") }";
        tapText.gameObject.SetActive(true);
        CoinIcon.transform.localScale = Vector3.one * 1.75f;

        AddGold(output);
    }

    private TapText GetOrCreateTapText(){
        TapText tapText = _tapTextPool.Find(t => !t.gameObject.activeSelf);
        
        if (tapText == null){
            tapText = Instantiate(TapTextPrefab).GetComponent<TapText>();
            _tapTextPool.Add(tapText);
        }

        return tapText;
    }

    // quit game on button press
    public void QuitGame(){
        Application.Quit();
    }
}

// System.Serializable makes object to become serializable and values can be set from the inspector
[System.Serializable]
public struct ResourceConfig{
    public string Name;
    public double UnlockCost;
    public double UpgradeCost;
    public double Output;
}
